FROM alpine:latest

WORKDIR /var/www/html
RUN apk add --no-cache \
  curl \
  nginx \
  nano \
  zip \
  php8 \
  php8-ctype \
  php8-curl \
  php8-dom \
  php8-fpm \
  php8-gd \
  php8-intl \
  php8-mbstring \
  php8-mysqli \
  php8-opcache \
  php8-openssl \
  php8-phar \
  php8-session \
  php8-xml \
  php8-xmlreader \
  php8-zlib \
  php8-tokenizer \
  php8-pdo_sqlite \
  supervisor

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY app/docker/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY app/docker/php.ini /etc/php8/conf.d/custom.ini
COPY app/docker/nginx.conf /etc/nginx/nginx.conf
COPY app/docker/site.conf /etc/nginx/conf.d/default.conf

RUN adduser --disabled-password -g 1000 -u 1000 -s /bin/bash -G www-data www-data

COPY --chown=www-data:www-data app/ /var/www/html
RUN unzip vendor.zip
RUN chmod -R ug+w /var/www/html/storage /var/www/html/bootstrap

EXPOSE 80

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]