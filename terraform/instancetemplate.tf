resource "google_compute_instance_template" "docker-template" {
  name         = "docker-template"
  machine_type = "e2-small"

  disk {
    source_image = "debian-cloud/debian-11"
    auto_delete  = true
    disk_size_gb = 30
    boot         = true
  }

  network_interface {
    network = google_compute_network.main.self_link
    subnetwork = google_compute_subnetwork.private.self_link
  }

  metadata = {
    metadata_startup_script = file("${path.module}/startup.sh")
  }
}
